# IOC for CTL Box vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   Spk-000CDL:Vac-VEG-001
    *   Spk-000CDL:Vac-VGP-070
	*	Spk-000CDL:Vac-VGC-071
