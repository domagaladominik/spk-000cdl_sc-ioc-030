#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Spk-000CDL:Vac-VEG-001
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Spk-000CDL:Vac-VEG-001 , IPADDR = moxa-cdl-endbox.tn.esss.lu.se, PORT = 4001")

# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME =Spk-000CDL:Vac-VGP-070 , CHANNEL = C1, CONTROLLERNAME =Spk-000CDL:Vac-VEG-001 ")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME =Spk-000CDL:Vac-VGC-071 , CHANNEL = A1, CONTROLLERNAME =Spk-000CDL:Vac-VEG-001 ")
